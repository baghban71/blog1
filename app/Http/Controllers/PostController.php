<?php

namespace App\Http\Controllers;

use App\Http\Resources\PostCollectionResource;
use App\Http\Resources\PostResource;
use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        return new PostCollectionResource(request()->page != null ? Post::paginate(4) : Post::all());
    }

    public function show($id)
    {
        $post = Post::find($id);
        return new PostResource($post);
    }
}
